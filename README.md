# Rocci Framework
Framework Rocci para el desarrollo de sitios web

## Caracteristicas

 - Sistema de rutas (Guarda rutas en .json, Alto Router)
 - Sistema de Controladores
 - Motor de plantillas Twig 2.0
 - Validador de campos
 - Variables de configuracion establecidas globlamente
 - Crea subcarpetas para las vistas desde el Controlador
 - Sistema de conexion a DB mediante Medoo.in

## Instalación
Una vez clonado el repositorio se deben instalar las dependencias
```cmd
composer update
```

## Creación de Modelos y/o Controladores
Una vez creado el Modelo o Controlador en sus respectivas carpetas, se debe ejecutar el comando de composer para poder cargas las clases
```cmd
composer dump-autoload
```