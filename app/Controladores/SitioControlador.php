<?php
	namespace Controlador;
	use Modelo\Contacto as Contacto;
	/**
	* 
	**/
	class SitioControlador extends Controlador {
		
		function __construct() {
			parent::__construct();
			//$this->makeFolder('admin', './vistas/admin/');
		}

		public function index() {
			$Contacto = new Contacto();
			$this->vista('@admin/test', ['contactos' => $Contacto->select()]);
		}
	}