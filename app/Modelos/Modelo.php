<?php
	namespace Modelo;

	/**
	* 
	*/
	class Modelo {
		private $db;
		private $conditions = [];
		private $pagina = 0;

		function __construct($argumento = NULL) {
			$this->db = new \Medoo\Medoo(sistema('db'));
		}

		/*
		*
		*	INSERT
		*
		*/
		public function insert($campos = []) {
			$this->db->insert($this->tabla, $campos);
			return $this->db->id();
		}

		/*
		*
		*	UPDATE
		*
		*/
		public function update($data = []) {
			if($this->db->update($this->tabla, $data, $this->conditions)) {
				$this->conditions = [];
				return true;
			}
		}

		/*
		*
		*	DELETE
		*
		*/
		public function delete($data = []) {
			$r = $this->db->delete($this->tabla, $this->conditions);
			$this->conditions = [];
			return $r;
		}

		/*
		*
		*	SELECT
		*
		*/
		public function select($campos = '*') {
			$r = $this->db->select($this->tabla, $campos, $this->conditions);
			$this->conditions = [];
			return $r;
		}

		/*
		*
		*	GET
		*
		*/
		public function get($campos = '*') {
			$r = $this->db->get($this->tabla, $campos, $this->conditions);
			$this->conditions = [];
			return $r;
		}

		/*
		*
		*			WHERE
		*	https://medoo.in/api/where
		*
		*/
		public function condition($c = []) {
			$this->conditions = $c;
		}

		public function db() {
			return $this->db;
		}
	}