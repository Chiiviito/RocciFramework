<?php
	/**
	*	Este es el que lee las rutas creadas y crea su controlador si es que este no esta creado
	*/
	class CreadorControladores {
		private $archivo = "<?php\n\tnamespace Controlador;\n\t/**\n\t*	\n\t**/\n\tclass {{ nombreControlador }} extends Controlador { \n\n\t\tfunction __construct() {\n\t\t\tparent::__construct();\n\t\t}";
		private $controlador;

		function __construct($controlador, $metodos) {
			$this->controlador = $controlador;
			if (file_exists('./app/Controladores/' . $this->controlador . '.php')) return;
			$this->crearMetodos($metodos);
		}

		private function crearMetodos($metodos) {
			foreach ($metodos as $metodo) {
				$this->archivo .= "\n\t\tpublic function {$metodo}() {\n\t\t\t\n\t\t}\n";
			}
		}

		public function ejecutar() {
			if (file_exists('./app/Controladores/' . $this->controlador . '.php')) return;
			$this->archivo = str_replace("{{ nombreControlador }}", $this->controlador,$this->archivo);
			$this->archivo .= "\n\t}";
			nl2br(trim(preg_replace('/\s\s+/', ' ', $this->archivo)));
			file_put_contents('./app/Controladores/' . $this->controlador . '.php', $this->archivo);

			// Actualizar clases
			exec('composer dump-autoload');
		}
	}