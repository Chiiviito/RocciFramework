<?php
	/**
	*	Este es el que lee las rutas creadas y crea su controlador si es que este no esta creado
	*/
	class CreadorRequisitos {
		private $archivo = "<?php\n\tnamespace Requisito;\n\t/**\n\t*\n\tRequisitos que se deben cumplir antes de entrar a la URL\n\t*/\n\tclass {{ nombreRequisito }} {\n\t\tfunction __construct() {\n\t\t\t# code...\n\t\t}\n\t\tpublic function start(\$d = null) {\n\t\t\treturn true;\n\t\t}\n\t\tpublic function error(\$d = null) {\n\t\t\techo \"ERROR SIN PERMISO\";\n\t\t}\n\t}";

		private $controlador;

		function __construct($controlador) {
			$this->nombre = $controlador;
			if (file_exists('./app/Requisitos/' . $this->nombre . '.php')) return;
		}

		public function ejecutar() {
			if (file_exists('./app/Requisitos/' . $this->nombre . '.php')) return;
			$this->archivo = str_replace("{{ nombreRequisito }}", $this->nombre,$this->archivo);
			nl2br(trim(preg_replace('/\s\s+/', ' ', $this->archivo)));
			file_put_contents('./app/Requisitos/' . $this->nombre . '.php', $this->archivo);

			// Actualizar clases
			exec('composer dump-autoload');
		}
	}