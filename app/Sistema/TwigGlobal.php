<?php
	namespace Sistema;
	/**
	* 
	*/
	class TwigGlobal {
		private $twig;
		function __construct($twig) {
			$this->twig = $twig;
			$this->cargarVariablesGlobales();
			$this->cargarFuncionesGlobales();
		}

		private function cargarVariablesGlobales() {
			// Asset
			$this->twig->addGlobal('res', variable('ruta') . '/res');
		}

		private function cargarFuncionesGlobales() {

			$f = new \Twig_Function('ruta', function ($nombre = '', $parametros = []) {
				return ruta($nombre, $parametros);
			});
			$this->twig->addFunction($f);

		}
	}