<?php

	/*
	|
	|	Configuracion del sitio
	|
	*/

	$sistema = [
		'base'		=>	'',
		'idioma'	=>	'es',
		'desarrollo'=>	true,
	];

	$captcha = [
		'publico'	=>	'6Lc52BITAAAAAGp487C73fZtcP2L_SZOdVBnw5G9',
		'privado'	=>	'6Lc52BITAAAAAKda0CY37hNx4mrXZcQqq73htuGy'
	];

	$correo = [
		'host'		=>	'smtp.gmail.com',
		'cuenta'	=>	true,
		'usuario'	=>	'robertocalani94@gmail.com',
		'clave'		=>	'0000000',
	];

	/*
	|
	|	Configuracion de las conexiones
	|
	*/
	$db = [
		// required
		'database_type' => 'mysql',
		'database_name' => 'rocci',
		'server' => 'localhost',
		'username' => 'root',
		'password' => '',

		// [optional]
		'charset' => 'utf8',
		'port' => 3306,

		// [optional] Table prefix
		//'prefix' => 'PREFIX_',

		// [optional] Enable logging (Logging is disabled by default for better performance)
		'logging' => true,

		// [optional] MySQL socket (shouldn't be used with server and port)
		//'socket' => '/tmp/mysql.sock',

		// [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
		/*'option' => [
			PDO::ATTR_CASE => PDO::CASE_NATURAL
		],*/

		// [optional] Medoo will execute those commands after connected to the database for initialization
		/*'command' => [
			'SET SQL_MODE=ANSI_QUOTES'
		]*/
	];